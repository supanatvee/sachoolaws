var express = require('express');
var hbs = require('express-hbs');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/website/index');
var backoffice = require('./routes/backoffice/index');
var bot = require('./routes/bot/index');
var api = require('./routes/api/index');

var app = express();

// view engine setup
app.engine('hbs', hbs.express4({
    defaultLayout: __dirname + '/views/layout',
    layoutsDir: path.join(__dirname, 'views'),
    partialsDir: path.join(__dirname, 'views/partials')
}));
// app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// website
app.use('/', index);
// backoffice
app.use('/backoffice', backoffice);
// bot
app.use('/bot', bot);
// api
app.use('/api', api);

/** view helper **/
hbs.registerHelper("loop", function(value, options) {
    var accum = '';
    for (var i = 1; i <= value; ++i)
        accum += options.fn(i);
    return accum;
});

hbs.registerHelper("switch", function(value, options) {
    this._switch_value_ = value;
    var html = options.fn(this); // Process the body of the switch block
    delete this._switch_value_;
    return html;
});

hbs.registerHelper("case", function(value, options) {
    if (value == this._switch_value_) {
        return options.fn(this);
    }
});

hbs.registerHelper("equal", function(value1,value2, options) {
    if (value1 == value2) {
        return options.fn(this);
    }
});
hbs.registerHelper("notequal", function(value1,value2, options) {
    if (value1 != value2) {
        return options.fn(this);
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
