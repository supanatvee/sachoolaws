var express = require('express');
var lineLogin = require('line-login-spnv');
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var dbManager = require('database');
var multer = require('multer')
var storage = multer.memoryStorage()
var upload = multer({ storage: storage });
var router = express.Router();

// AWS
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./routes/api/config.json');
var s3 = new AWS.S3();
var myBucket = 'com.sachool.www';
// AWS


var uri = "mongodb://liveesoftewaredevelopment:1234@mycluster-shard-00-00-ijs3v.mongodb.net:27017,mycluster-shard-00-01-ijs3v.mongodb.net:27017,mycluster-shard-00-02-ijs3v.mongodb.net:27017/database?ssl=true&replicaSet=myCluster-shard-0&authSource=admin";

router.route('/')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getLvListAll(db, function(data) {
                    db.close();
                    // console.log(data);
                    res.render('./backoffice/index', { title: "Sachool", data: data });
                });
            });
        });

router.route('/add-level')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getLvListAll(db, function(data) {
                    db.close();
                    // console.log(data);
                    res.render('./backoffice/add-level', { title: "Sachool", data: data });
                });
            });
        })
    .post(
        upload.single('img'),
        function(req, res, next) {
            var fileName = 'level-profile';
            MongoClient.connect(uri, function(err, db) {
                params = { Bucket: myBucket + '/subjects/' + req.body.subject + '/' + req.body.level, Key: fileName, Body: req.file.buffer, ACL: 'public-read-write' };
                s3.putObject(params, function(err, data) {
                    if (err) {
                        console.log(err)
                        res.redirect('/backoffice');
                    } else {
                        console.log(req.file);
                        console.log(data);
                        dbManager.addLevel(req.body.subject, req.body.level, req.body.description, 'https://s3-ap-southeast-1.amazonaws.com/' + myBucket + '/subjects/' + req.body.subject + '/' + req.body.level + '/' + fileName, db, function(data) {
                            db.close();
                            console.log("Successfully uploaded data");
                            res.redirect('/backoffice');
                        });
                    }

                });
            });
        });

router.route('/edit-level/:subject/:level')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getLvDetail(req.params.level, db, function(data) {
                    db.close();
                    console.log(data);
                    res.render('./backoffice/edit-level', { title: "Sachool", data: { subject: req.params.subject, lv: data.level[0] } });
                });
            });
        })
    .post(
        upload.single('img'),
        function(req, res, next) {
            var fileName = 'level-profile';
            MongoClient.connect(uri, function(err, db) {
                params = { Bucket: myBucket + '/subjects/' + req.body.subject + '/' + req.body.level, Key: fileName, Body: req.file.buffer, ACL: 'public-read-write' };
                s3.putObject(params, function(err, data) {
                    if (err) {
                        console.log(err)
                        res.redirect('/backoffice');
                    } else {
                        console.log(req.file);
                        console.log(data);
                        dbManager.editLevel(req.body.level, req.body.description, 'https://s3-ap-southeast-1.amazonaws.com/' + myBucket + '/subjects/' + req.body.subject + '/' + req.body.level + '/' + fileName, db, function(data) {
                            db.close();
                            console.log("Successfully uploaded data");
                            res.redirect('/backoffice');
                        });
                    }

                });
            });
        });

router.route('/add-exam/:sub/:lv')
    .get(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getLvListAll(db, function(data) {
                    db.close();
                    // console.log(data);
                    res.render('./backoffice/add-exam', {
                        title: "Sachool",
                        data: {
                            subject: req.params.sub,
                            level: req.params.lv,
                            username: 'admin'
                        }
                    });
                });
            });
        })
    .post(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.addExam(req.body.subject, req.body.level, req.body.title, req.body.detail, req.body.time, req.body.price, req.body.postBy, db, function(data) {
                    db.close();
                    // console.log(data);
                    res.redirect('/backoffice/category/' + req.body.subject + '/' + req.body.level);
                });
            });
        })
    .delete(
        function(req, res, next) {
            console.log(req.body);
            MongoClient.connect(uri, function(err, db) {
                dbManager.deleteExam(req.body.exam_id, req.body.level, db, function(data) {
                    console.log(data)
                    db.close();
                    res.send(data);
                });
            });
        });

router.route('/edit-element/:elementId')
    .get(function(req, res, next) {
        MongoClient.connect(uri, function(err, db) {
            dbManager.getElement(req.params.elementId, db, function(data) {
                db.close();
                console.log(data);
                res.render('./backoffice/edit-element', {
                    title: "Sachool",
                    data: data,
                    username: 'admin'
                });
            });
        });
    })
    .post(function(req, res, next) {
        MongoClient.connect(uri, function(err, db) {
            buildUpdateElement(req.body, function(obj) {
                dbManager.editElement(req.params.elementId, obj.detail, obj.check, db, function(data) {
                    res.redirect('/backoffice/view-element/' + req.body.exam_id + '/1');
                });
            });
        });
        // console.log(req.body);
        // res.end();
    })
    .delete(function(req, res, next) {
        MongoClient.connect(uri, function(err, db) {
            dbManager.deleteElement(req.params.elementId, db, function(data) {
                res.send(data);
            });
        });
    });

router.route('/add-element/:examId')
    .get(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getQuListByExamId(req.params.examId, db, function(data) {
                    db.close();

                    var order;

                    if (data.length > 0) {
                        order = (data[data.length - 1].order + 1);
                    } else {
                        order = 0;
                    }

                    // console.log(data);
                    res.render('./backoffice/add-element', {
                        title: "Sachool",
                        data: {
                            exam_id: req.params.examId,
                            order: order,
                            type: req.query.subject,
                            username: 'admin'
                        }
                    });
                });
            });
        })
    .post(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                var element = {};

                element.exam_id = req.body.exam_id;
                element.order = parseInt(req.body.order);
                element.type = req.body.type;

                switch (element.type) {
                    case 'question-choice':
                        var choice = [];

                        choice.push({ text: req.body['0'] });
                        choice.push({ text: req.body['1'] });
                        choice.push({ text: req.body['2'] });
                        choice.push({ text: req.body['3'] });

                        element.detail = {
                            question: req.body.question,
                            choice: choice
                        };

                        element.check = {
                            answer: parseInt(req.body.answer),
                            score: 1
                        };

                        // console.log(element);

                        dbManager.addElement(element.exam_id, element.order, element.detail, element.type, element.check, db, function(data) {
                            db.close();
                            // console.log(data);
                            res.redirect('/backoffice/view-element/' + req.params.examId + '/1');
                        });

                        break;
                    case 'topic':
                        element.detail = {
                            text: req.body.text
                        };

                        element.check = null;

                        dbManager.addElement(element.exam_id, element.order, element.detail, element.type, element.check, db, function(data) {
                            db.close();
                            // console.log(data);
                            res.redirect('/backoffice/view-element/' + req.params.examId + '/1');
                        });

                        break;
                }
            });
        });

router.route('/view-element/:examId/:page')
    .get(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getExamDetail(req.params.examId, db, function(profile) {
                    dbManager.getQuListByExamId(req.params.examId, db, function(data) {
                        db.close();
                        // console.log(data);

                        var perPageLimit = 20;
                        var amountPage = Math.ceil(data.length / perPageLimit);
                        var start = req.params.page * perPageLimit - perPageLimit;
                        var end = start + perPageLimit;
                        var pageElement = data.slice(start, end);

                        res.render('./backoffice/view-element', {
                            title: "Sachool",
                            data: {
                                exam_id: req.params.examId,
                                amount_page: amountPage,
                                exam_list: pageElement,
                                profile: profile
                            }
                        });
                    });
                });
            });
        })
    .post(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.editExam(req.params.examId, req.body.exam_title, req.body.exam_detail, req.body.exam_price, req.body.exam_time, db, function(data) {
                    db.close()
                    res.redirect('/backoffice/view-element/' + req.params.examId + '/' + req.params.page);
                });
            });
        });

router.route('/category/:sub/:lv')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getUserByLineId(req.cookies.userId, db, function(profile) {
                    dbManager.getLvDetail(req.params.lv, db, function(subject) {
                        dbManager.getExListByLevel(req.params.lv, req.params.sub, db, function(examList) {
                            dbManager.getReviewByLv(req.params.sub, req.params.lv, db, function(reviews) {
                                dbManager.getBroughtExamByLv(req.cookies.userId, req.params.lv, db, function(assets) {
                                    db.close();
                                    async.map(examList, function(item, callback) {
                                        item.brought = false;
                                        for (var i = 0, len = assets.length; i < len; i++) {
                                            if (item._id.toString() == assets[i].detail.asset_id) {
                                                item.brought = true;
                                                break;
                                            }
                                        }
                                        callback(null, item);
                                    }, function(err, examList) {
                                        res.render('./backoffice/ex-cat', {
                                            title: "Sachool",
                                            data: {
                                                subject: req.params.sub,
                                                level: req.params.lv,
                                                user: {
                                                    username: (profile == null ? 'Guest' : profile.name),
                                                    coin: (profile == null ? 0 : profile.coin)
                                                },
                                                detail: {
                                                    title: subject.level[0].title,
                                                    description: subject.level[0].description,
                                                    img_url: subject.level[0].img_url
                                                },
                                                exam_list: examList,
                                                reviews: reviews
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

module.exports = router;

function buildUpdateElement(_object, callback) {
    switch (_object.type) {
        case 'topic':
            var detail = {};
            var check = {};
            var object = {};

            detail.text = _object.text;
            check = null;

            object.detail = detail;
            object.check = check;

            callback(object);

            break;
        case 'question-choice':
            var detail = {};
            var check = {};
            var object = {};

            detail.question = _object.question;
            detail.choice = [];
            detail.choice.push({ text: _object['0'] });
            detail.choice.push({ text: _object['1'] });
            detail.choice.push({ text: _object['2'] });
            detail.choice.push({ text: _object['3'] });
            check.answer = parseInt(_object.answer);
            check.score = parseInt(_object.score);

            object.detail = detail;
            object.check = check;

            callback(object);
            break;
    }
}
