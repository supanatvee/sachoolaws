var express = require('express');
var lineLogin = require('line-login-spnv');
// var linePay = require('line-pay-spnv');
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var dbManager = require('database');
var router = express.Router();

var login = new lineLogin({
    name: 'David (website)',
    channelId: '1522746723',
    channelSecret: '96075562aeea076daa8d8bd36f594ece'
});

var uri = "mongodb://liveesoftewaredevelopment:1234@mycluster-shard-00-00-ijs3v.mongodb.net:27017,mycluster-shard-00-01-ijs3v.mongodb.net:27017,mycluster-shard-00-02-ijs3v.mongodb.net:27017/database?ssl=true&replicaSet=myCluster-shard-0&authSource=admin";

router.route('/')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getLvListAll(db, function(data) {
                    db.close();
                    console.log(data);
                    res.render('./website/index', { title: "Sachool", data: data });
                });
            });
        });

router.route('/profile')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getUserByLineId(req.cookies.userId, db, function(data) {
                    db.close();
                    res.render('./website/user-profile', {
                        title: 'Profile',
                        data: data
                    });
                });
            });
        });

router.route('/edit-profile')
    .get(
        login.isLogin({
            response_type: 'code',
            redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
            state: 'abc1412'
        }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getUserByLineId(req.cookies.userId, db, function(data) {
                    db.close();
                    res.render('./website/edit-profile', {
                        title: 'Profile',
                        data: data
                    });
                });
            });
        })
    .post(
        login.isLogin({
            response_type: 'code',
            redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
            state: 'abc1412'
        }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                console.log(req.body);
                dbManager.updateProfile(req.cookies.userId, req.body.usr, db, function(data) {
                    db.close();
                    res.redirect('/');
                });
            });
        });

router.route('/exam/:id/edit')
    .get(
        login.isLogin({
            response_type: 'code',
            redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
            state: 'abc1412'
        }),
        function(req, res, next) {
            var data = { title: 'Website', id: req.cookies.userId };
            res.render('./website/ex-edit', data);
        });

router.route('/exam/:id/answer-sheet')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getBroughtExamById(req.cookies.userId, req.params.id, db, function(users) {
                    dbManager.getExamDetail(req.params.id, db, function(profile) {
                        dbManager.getQuListByExamId(req.params.id, db, function(data) {
                            db.close();
                            // if (users == null || users == undefined ) {
                                // res.redirect('/');
                            // } else
                                res.render('./website/answer-sheet', { title: "Sachool", data: data, profile: profile });
                        });
                    });
                });
            });
        });

router.route('/exam/:id/do')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getExamDetail(req.params.id, db, function(profile) {
                    dbManager.getQuListByExamId(req.params.id, db, function(data) {
                        db.close();
                        res.render('./website/ex-do', { title: "Sachool", data: data, profile: profile });
                    });
                });
            });
        })
    .post(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            console.log(req.body)
            MongoClient.connect(uri, function(err, db) {
                dbManager.getExamDetail(req.params.id, db, function(detail) {
                    dbManager.getScoreAbleElementByExam(req.params.id, db, function(data) {
                        db.close();
                        var score = 0;
                        async.map(data, function(item, callback) {
                            var isCorrect = false;
                            if (item.check.answer == req.body[item._id]) {
                                isCorrect = true;
                                score += item.check.score;
                            }
                            callback(null, isCorrect)
                        }, function(err, result) {
                            res.render('./website/ex-done', {
                                title: "Sachool",
                                data: {
                                    exam_id: req.params.id,
                                    sub: detail.subject,
                                    level: detail.level,
                                    score: score,
                                    total: result.length
                                }
                            });
                        });
                    });
                });
            });
        });

router.route('/category/:sub/:lv')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.getUserByLineId(req.cookies.userId, db, function(profile) {
                    dbManager.getLvDetail(req.params.lv, db, function(subject) {
                        dbManager.getExListByLevel(req.params.lv, req.params.sub, db, function(examList) {
                            dbManager.getReviewByLv(req.params.sub, req.params.lv, db, function(reviews) {
                                dbManager.getBroughtExamByLv(req.cookies.userId, req.params.lv, db, function(assets) {
                                    db.close();
                                    async.map(examList, function(item, callback) {
                                        item.brought = false;
                                        for (var i = 0, len = assets.length; i < len; i++) {
                                            if (item._id.toString() == assets[i].detail.asset_id) {
                                                item.brought = true;
                                                break;
                                            }
                                        }
                                        callback(null, item);
                                    }, function(err, examList) {
                                        res.render('./website/ex-cat', {
                                            title: "Sachool",
                                            data: {
                                                subject: req.params.sub,
                                                level: req.params.lv,
                                                user: (profile == null ? { username: 'Guest', coin: 0 } : profile),
                                                detail: {
                                                    title: subject.level[0].title,
                                                    description: subject.level[0].description,
                                                    img_url: subject.level[0].img_url
                                                },
                                                exam_list: examList,
                                                reviews: reviews
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

router.route('/exam/done')
    .post(
        login.isLogin({
            response_type: 'code',
            redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
            state: 'abc1412'
        }),
        function(req, res, next) {
            // do a result
            var exam = req.body
            var correct = 0;
            var percent = 0;
            var theshold = 50;
            var result = false;

            for (var key in exam) {
                if (exam[key] == "true") {
                    correct++;
                }
            }

            console.log(exam);

            percent = correct / Object.keys(exam).length * 100;

            if (percent > theshold)
                result = true;

            var info = {
                exam_count: Object.keys(exam).length,
                correct: correct,
                percent: percent.toFixed(2),
                result: result
            };

            var data = { title: 'Result', data: info };
            res.render('./website/ex-done', data);
        });

router.route('/test-form')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            var data = { title: 'Website', id: req.cookies.userId };
            res.render('./website/test-form', data);
        })
    .post(
        function(req, res, next) {

            var exam = req.body
            var result = 0;

            for (var key in exam) {
                if (exam[key] == "true") {
                    result++;
                }
            }

            res.send({ data: result });
        });

router.route('/test-add-quest/:sub/:id')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            var data = { title: 'Add question', exam_sub: req.params.sub, exam_id: req.params.id };
            res.render('./website/test-add-quest', data);
        })
    .post(
        function(req, res, next) {

            // add data 

            var data = { title: 'Website', id: req.cookies.userId };
            res.render('./website/test-add-quest', data);
        });

router.route('/test-add-exam')
    .get(
        login.isLogin({
            response_type: 'code',
            redirect_uri: 'https://sachoolaws.herokuapp.com/api/login-hook',
            state: 'abc1412'
        }),
        function(req, res, next) {
            var data = { title: 'Create exam', id: req.cookies.userId };
            res.render('./website/test-create-exam', data);
        })
    .post(
        function(req, res, next) {

            // add data 

            var data = { title: 'Website', id: req.cookies.userId };
            res.render('./website/test-add-quest', data);
        });

router.route('/test-index')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            examManager.viewallexam(function(examdata) {
                userManager.viewuserdata(req.cookies.userId, function(userdata) {
                    var subject = userdata.studyingsub;
                    var line_id = userdata.line_id;
                });
                res.cookie('userId', req.cookies.userId);
                res.render('./website/test-index', { title: "Sachool", data: examdata });
            });
        });

// router.route('/category/:sub/:rank')
//     .get(
//         // login.isLogin({
//         //     response_type: 'code',
//         //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
//         //     state: 'abc1412'
//         // }),
//         function(req, res, next) {
//             examManager.viewexamsetbyrank(req.params.sub, req.params.rank, function(examdata) {
//                 res.cookie('userId', req.cookies.userId);
//                 res.render('./website/ex-list-rank', { title: "Sachool", data: examdata });
//             });
//         });

router.route('/test-upload')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {
            // examManager.viewallexam(function(examdata) {
            // res.cookie('userId', req.cookies.userId);
            res.render('./website/test-upload', { title: "Sachool", data: 'examdata' });
            // });
        })
    .post(
        function(req, res, next) {

            var exam = req.body
            var result = 0;

            for (var key in exam) {
                if (exam[key] == "true") {
                    result++;
                }
            }

            res.send({ data: result });
        });

router.route('/test-exam/:sub/:id/do')
    .get(
        // login.isLogin({
        //     response_type: 'code',
        //     redirect_uri: 'https://sachool.herokuapp.com/api/login-hook',
        //     state: 'abc1412'
        // }),
        function(req, res, next) {

            examManager.viewexamsetbyexamId(req.params.sub, req.params.id, function(examdata) {
                var data = {
                    title: 'Exam',
                    data: JSON.stringify(examdata)
                };
                res.render('./website/test-index', data);
            });
        });

module.exports = router;
