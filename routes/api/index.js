var express = require('express');
var lineLogin = require('line-login-spnv');
var router = express.Router();
var async = require('async');
var request = require('request').defaults({ encoding: null });

// AWS
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./routes/api/config.json');
var s3 = new AWS.S3();
var myBucket = 'com.sachool.www';
// AWS

var MongoClient = require('mongodb').MongoClient;
var dbManager = require('database');

var uri = "mongodb://liveesoftewaredevelopment:1234@mycluster-shard-00-00-ijs3v.mongodb.net:27017,mycluster-shard-00-01-ijs3v.mongodb.net:27017,mycluster-shard-00-02-ijs3v.mongodb.net:27017/database?ssl=true&replicaSet=myCluster-shard-0&authSource=admin";

var login = new lineLogin({
    name: 'David (API)',
    channelId: '1522746723',
    channelSecret: '96075562aeea076daa8d8bd36f594ece'
});

router.route('/update-count')
    .get(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.countQuestion('JPN', 'N5', db, function(data) {
                    console.log(data);
                    db.close();
                    res.end();
                });
            });
        });

router.get('/login-hook', function(req, res, next) {
    login.signIn(req.query.code,
        req.query.state,
        'https://sachoolaws.herokuapp.com/api/login-hook',
        function(data) {
            console.log(data);
            MongoClient.connect(uri, function(errDb, db) {
                console.log(req.cookies);
                res.cookie('userId', data.userId);
                dbManager.getUserByLineId(data.userId, db, function(profile) {
                    if (profile == null) {
                        request.get(data.pictureUrl, function(errR, result, body) {
                            var fileName = 'user-profile';
                            params = { Bucket: myBucket + '/users/' + data.userId, Key: fileName, Body: body, ACL: 'public-read-write' };
                            s3.putObject(params, function(errS3, dataS3) {
                                if (err) {
                                    // console.log(err)
                                    res.redirect('/backoffice');
                                } else {
                                    dbManager.addUser(data.userId,
                                        data.displayName,
                                        'https://s3-ap-southeast-1.amazonaws.com/' + myBucket + '/users/' + data.userId + '/' + fileName, db,
                                        function(added) {
                                            db.close();
                                            res.redirect('/edit-profile');
                                        });
                                }

                            });
                        });
                    } else {
                        res.redirect('/');
                    }
                });
            });
        });
});

router.get('/logout', function(req, res, next) {
    res.clearCookie('userId');
    res.redirect('/');
});

router.route('/create-exam')
    .post(function(req, res, next) {
        userManager.viewuserpermission(req.cookies.userId, function(permission) {
            var pushObj = examManager.createexamset(
                req.body.title,
                req.body.rank,
                req.body.price,
                req.body.detail,
                req.body.timelimit
            );
            var examkey = examManager.pushexamset(
                req.body.sub,
                req.cookies.userId,
                pushObj
            );
            res.send(examkey);
        });
    });

router.route('/exam/:sub/:examid')
    .get(function(req, res, next) {
        examManager.viewexamsetbyexamId(req.params.sub, req.params.examid, function(data) {
            res.send(data);
        });
    })
    .post(function(req, res, next) {

        // var pushObj = exam.createpushObj("JPN00", questionlist, "ภาษาญี่ปุ่นตัวที่ 9");
        // examManager.createexamset("jpn", data.source.userId, pushObj);

        // examManager.viewexamdata("jpn", "JPN000", function(examdata) {
        //     var choice = [];
        //     for (var i = 1; i <= 4; i++) {
        //         choice[i] = "choice" + i;
        //     }
        //     choice.correct = 2;
        //     var newquestionlist = examManager.createquestionlist(choice, "null", "quest", "rank");
        //     var questionlist = examdata.question_list;
        //     questionlist.push(newquestionlist)
        //     var alterObj = examManager.alterquestionlist(questionlist);
        //     examManager.alterexamset("jpn", examdata.examkey, alterObj);

        // });

    });

router.route('/create-user')
    .get(function(req, res, next) {
        var pushObj = userManager.createpushObj(
            req.body.post_permission,
            req.body.buy_permission,
            req.body.day_exam,
            "jpn",
            req.body.name);
        userManager.adduser(req.cookies.userId, pushObj);
        res.send({
            success: true
        });
    });

router.route('/add-question')
    .post(function(req, res, next) {
        userManager.viewuserpermission(req.cookies.userId, function(permission) {
            var allkey = {
                examkey: req.body.examkey,
                sub: req.body.subject
            };

            var choiceObj = examManager.createchoiceObj(JSON.parse(req.body.text), req.body.correct, req.body.quest);
            res.send(examManager.pushinstanctquestionlist(allkey, choiceObj));
        });
    });

router.route('/alter-coin')
    .patch(function(req, res, next) {
        userManager.alterusercoin(req.cookies.userId, req.body.coin, function(result) {
            res.send(result);
        });
    });

router.route('/add-asset')
    .post(function(req, res, next) {
        console.log(req.body);
        MongoClient.connect(uri, function(err, db) {
            dbManager.addAsset(req.body.cat, req.body.rela, req.cookies.userId, req.body.examId, req.body.sub, req.body.level, db, function(data) {

                var paid = parseInt(req.body.price);

                dbManager.getUserByLineId(req.cookies.userId, db, function(profile) {
                    if (profile.coin >= paid) {
                        dbManager.updateCoinUser(req.cookies.userId, paid * -1, db, function(data) {
                            db.close();
                            res.send({ success: true });
                        });
                    } else {
                        db.close();
                        res.send({ success: false });
                    }
                });
            });
        });
    });

router.route('/add-review')
    .post(function(req, res, next) {
        console.log(req.body);
        MongoClient.connect(uri, function(err, db) {
            dbManager.getUserByLineId(req.cookies.userId, db, function(profile) {
                dbManager.addReviewToEx(req.body.sub, req.body.lv, req.body.exam_id, profile.line_id, profile.name, parseInt(req.body.rating), req.body.text, profile.imageUrl, db, function(data) {
                    db.close();
                    res.send(data);
                });
            });
        });
    });

router.route('/delete-level')
    .delete(
        function(req, res, next) {
            console.log(req.body);
            MongoClient.connect(uri, function(err, db) {
                dbManager.deleteLevel(req.body.subject, req.body.level, db, function(data) {
                    console.log(data)
                    db.close();
                    res.send(data);
                });
            });
        });

router.route('/unlockall/:sub/:level')
    .post(function(req, res, next) {
        MongoClient.connect(uri, function(err, db) {
            dbManager.getUserByLineId(req.cookies.userId, db, function(profile) {
                dbManager.getExListByLevel(req.params.level, req.params.sub, db, function(exlist) {
                    dbManager.getBroughtExamByLv(req.cookies.userId, req.params.level, db, function(assets) {
                        var total_price = 0;
                        async.map(exlist, function(item, callback) {
                            total_price += item.price;
                            var newItem = item;
                            for (var i = 0, len = assets.length; i < len; i++) {
                                if (item._id.toString() == assets[i].detail.asset_id) {
                                    newItem = {};
                                    total_price -= item.price;
                                    break;
                                }
                            }
                            callback(null, newItem);
                        }, function(err, result) {

                            if (total_price > profile.coin) {
                                console.log('Not enogh coin');
                                res.send({ success: false, message: 'Not enogh coin' });

                            } else {
                                console.log('Enogh coin');
                                async.map(result, function(item, callback) {
                                    if (item._id != null) {
                                        dbManager.addAsset('exams', 'buy', req.cookies.userId, item._id, req.params.sub, req.params.level, db, function(data) {
                                            callback(null, true)
                                        });
                                    } else {
                                        callback(null, false)
                                    }
                                }, function(err, result2) {
                                    console.log('result : ');
                                    console.log(result2)
                                    console.log('total price : ' + total_price.toString());
                                    dbManager.updateCoinUser(req.cookies.userId, (total_price) * -1, db, function(updated) {
                                        db.close();
                                        res.send({ success: true, message: 'success' });
                                    });
                                });
                            }
                        });
                    });
                });
            });
        });
    });

router.route('/countEachExam')
    .get(
        function(req, res, next) {
            MongoClient.connect(uri, function(err, db) {
                dbManager.countEachLv(db, function(examList) {
                    db.close();
                    console.log(examList);
                    res.end();
                    // async.map(examList, function(item, callback) {
                    //     item.brought = false;
                    //     for (var i = 0, len = assets.length; i < len; i++) {
                    //         if (item._id.toString() == assets[i].detail.asset_id) {
                    //             item.brought = true;
                    //             break;
                    //         }
                    //     }
                    //     callback(null, item);
                    // }, function(err, examList) {
                    //     res.render('./backoffice/ex-cat', {
                    //         title: "Sachool",
                    //         data: {
                    //             subject: req.params.sub,
                    //             level: req.params.lv,
                    //             user: {
                    //                 username: profile.name,
                    //                 coin: profile.coin
                    //             },
                    //             detail: {
                    //                 title: subject.level[0].title,
                    //                 description: subject.level[0].description,
                    //                 img_url: subject.level[0].img_url
                    //             },
                    //             exam_list: examList,
                    //             reviews: reviews
                    //         }
                    //     });
                    // });
                });
            });
        });

module.exports = router;
