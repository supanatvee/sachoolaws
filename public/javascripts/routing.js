function toDoExam(_subject, _examId) {
    window.location.href = '/exam/' + _subject + '/' + _examId + '/do';
}

function toAddQuestion(_subject, _examId) {
    window.location.href = '/test-add-quest/' + _subject + '/' + _examId;
}
