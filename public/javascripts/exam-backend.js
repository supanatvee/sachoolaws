function createExam(_sub, _name, _rank, _price, _imgePath, _detail, _timelimit) {
    $.ajax({
        type: 'POST',
        url: "/api/create-exam",
        data: {
            "sub": _sub,
            "title": _name,
            "rank": _rank,
            "price": _price,
            "image_path": _imgePath,
            "detail": _detail,
            "timelimit": _timelimit
        },
        success: function(_key) {
            // console.log(_key);
            // console.log(_key.sub);
            // var _key = JSON.parse(key);
            toAddQuestion(_key.sub, _key.examkey);
        }
    });
}

function addQuestion(_sub, _examId, _questionObj) {
    $.ajax({
        type: 'POST',
        url: "/api/add-question",
        data: {
            "examkey": _examId,
            "subject": _sub,
            "text": JSON.stringify(_questionObj.text),
            "correct": _questionObj.correct,
            "quest": _questionObj.quest
        },
        success: function(_key) {
            // toDoExam(_sub, _key);
            alert("Question was added");
        }
    });
}

function reloadQuestionList(_examId) {
    // reload

    // new data to element
}
