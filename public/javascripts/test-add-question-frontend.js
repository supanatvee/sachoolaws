function retriveQuestionData(_sub, _id) {
    var choice = [];

    var question = document.getElementById("question").value;
    var corr = document.querySelector('input[name="correct"]:checked').value;
    choice.push(document.getElementById("choice1").value);
    choice.push(document.getElementById("choice2").value);
    choice.push(document.getElementById("choice3").value);
    choice.push(document.getElementById("choice4").value);

    var questObj = {
        text: choice,
        correct: corr,
        quest: question
    };
    
    addQuestion(_sub, _id, questObj);
}
