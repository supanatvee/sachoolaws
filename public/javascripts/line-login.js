// Defiine
var LINE_LOGIN_HOOK = "https://sachool.herokuapp.com/api/login-hook";
var LINE_LOGOUT = "https://sachool.herokuapp.com/api/logout";
var LINE_LOGIN_CLIENTID = "1510166957";

// Pair UI
var lineLoginAction = document.getElementsByClassName("line-login-action");
var lineLogoutAction = document.getElementsByClassName("line-logout-action");

// Set onclick
for (var i = lineLoginAction.length - 1; i >= 0; i--) {
    lineLoginAction[i].addEventListener('click', lineLogin);
}
for (var i = lineLogoutAction.length - 1; i >= 0; i--) {
    lineLogoutAction[i].addEventListener('click', lineLogout);
}

// Activity
function lineLogin() {
    window.location = "https://access.line.me/dialog/oauth/weblogin?response_type=code&client_id=" + LINE_LOGIN_CLIENTID +
        "&redirect_uri=" + LINE_LOGIN_HOOK +
        "&state=abc1412";
}
function lineLogout() {
    window.location = LINE_LOGOUT;
}
